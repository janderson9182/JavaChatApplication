#Simple Java chat application 
 - Client and up to 4 servers
 - This code would be reusable to make a server on a remote server for example
 - Inspiration for this came when I was told that I needed to make one for an assignment ;)
 - One thing I know it is missing is to close the sockets
 - To use this you need to compile server.java and client.java and then run the server and then the 4 clients. 
 - You should run a client and then log in (Usernames and passwords are in an array in server.java (easy solution) before opening the next client. 
 - You can send a message to everyone or to an individual or send a message to everyone from the server. 
