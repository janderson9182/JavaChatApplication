
import java.io.Serializable;
import java.util.ArrayList;
/**
 * 
 * @author James Anderson 
 *
 */
public class CompressedMessage implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	// this instance variable will store the original, compressed and decompressed message
	private String message;

	public CompressedMessage(String message)
	{	
		// begin by coding this method first - initialise instance variable message with the original message
		this.message = message;
	}

	public String getMessage()
	{	
		return message.trim();//removes spaces at start and end of text
	}

	private boolean punctuationChar(String str)
    {   
		// check if the last character in the string is a punctuation
       	return (str.charAt(str.length() - 1) == ',' || str.charAt(str.length() - 1) == '.' || str.charAt(str.length() - 1) == '?');
    }

	private String getWord(String str)
   	{   
		// If last character in string is punctuation then remove 
   		if (punctuationChar(str))
   		{
   			str = str.substring(0, str.length() - 1);
   		}
   		return str;
   	}
	
	
	public void compress()
	{	
		ArrayList<String> dictionary = new ArrayList<String>();
		String compressedStr = "";

		// split message string into words, using space character as delimiter
		String[] words = message.split(" \\s*");
		for(int i = 0; i < words.length; i++)
		{	int foundPos = dictionary.indexOf(getWord(words[i]));
			if(foundPos == -1)
  			{   // word is not found therefore add to end of array list
   				dictionary.add(getWord(words[i]));
         		// add word to compressed message
    			compressedStr += getWord(words[i]);
        	}
       		else
   				/* match found in array list - add corresponding position
    			   of word to compressed message */
     			compressedStr += foundPos;

    		if(punctuationChar(words[i]))
     			compressedStr += words[i].charAt(words[i].length()-1);
           	compressedStr += " ";
		}

    		// store compressed message in instance variable
  		message = compressedStr;
  	}

	public void decompress()
	{	
		ArrayList<String> dictionary = new ArrayList<String>();
		String decompressedString = "";
		int position;

		// split message string into words, using space character as delimiter
		String[] words = message.split(" \\s*");
		for(int i = 0; i < words.length; i++)
		{	// test if the first character of this string is a digit
			if (words[i].charAt(0) >= '0' && words[i].charAt(0) <= '9')
			{	/* it is a digit - this indicates that this string represents
				   the position of a word in the previous words list.
				   convert this string to an int value */
				position = Integer.parseInt(getWord(words[i]));
				// get word at this position & add to decompressed message
				decompressedString += dictionary.get(position);
			}
			else
			{	// this string is a word - add to previous words list
				dictionary.add(getWord(words[i]));
				// add word to compressed message
				decompressedString += getWord(words[i]);
			}

			if(punctuationChar(words[i]))
         		decompressedString += words[i].charAt(words[i].length()-1);
         	decompressedString += " ";
		}

		// store decompressed message in instance variable
   		message = decompressedString;
	}
}