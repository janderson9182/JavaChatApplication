import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * The client program for the aplication 
 * @author James Anderson 
 *
 */
public class Client extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	/*
	 * This string will be used to terminate the connection
	 */
	final String DELIMINATOR = "> ";
	
	//Network variables 
	Socket socket;
	ObjectInputStream clientInputStream;
	ObjectOutputStream clientOutputStream;

	// variables for the GUI components of the chat client 
	Container container; 
	JButton logonButton;
	ButtonHandler bHandler;
   	JPanel friendPanel, buttonPanel, messagePanel, outputPanel, logonFieldsPanel, logonButtonPanel;
   	JLabel usernameLabel, passwordLabel;
   	JTextArea outputArea, receiveMessageArea, sendMessageArea;
   	JTextField username;
   	JPasswordField password;
   	JButton arkenButton, benButton, darklarkButton, freeButton, groupButton, send;	
   	
	ArrayList<String> onlineFriends = new ArrayList<String>();
	
	String selectedButton;
   	
	public Client()
	{	
		super("Chat Service - Client");
		addWindowListener
		(	
				new WindowAdapter()
			{	
					public void windowClosing(WindowEvent e)
				{	
						System.exit(0);
				}
			}
		);
      
      	/* the initial GUI will provide a text field and password field 
      	   to enable the user to enter their username and password and 
      	   attempt to logon to the chat client */
      	   
      	// create and add GUI components
   		container = getContentPane(); 
   		container.setLayout(new BorderLayout());  
      	   
      	// GUI components for the username
      	logonFieldsPanel = new JPanel(); 
		logonFieldsPanel.setLayout(new GridLayout(2,2,5,5));
		usernameLabel = new JLabel("Enter Username: ");
		logonFieldsPanel.add(usernameLabel);
		username = new JTextField(10);
		logonFieldsPanel.add(username);
		
		// GUI components for the password
		passwordLabel = new JLabel("Enter Password: ");
		logonFieldsPanel.add(passwordLabel);
		password = new JPasswordField(10);
		logonFieldsPanel.add(password);
		container.add(logonFieldsPanel,BorderLayout.CENTER);
		
		// panel for the logon button
		logonButtonPanel = new JPanel();
		logonButton = new JButton("logon");
		bHandler = new ButtonHandler();
		logonButton.addActionListener(bHandler);
		logonButtonPanel.add(logonButton);
		container.add(logonButtonPanel, BorderLayout.SOUTH);
		
		setSize(300,125);
		setResizable(false);
		setVisible(true);
	}
	
	void getConnections()
	{	
		try
		{	
			/* 	create a socket and get a connection to server,
				get input & output streams */
			socket = new Socket(InetAddress.getLocalHost(), 6000);
			
			clientOutputStream = new ObjectOutputStream(socket.getOutputStream());
			clientInputStream = new ObjectInputStream(socket.getInputStream());
			
			/* create a new thread of ClientThread, sending input  
			   stream variable as a parameter and remember to start a 
			   new thread of execution for ClientThread*/
			ClientThread thread = new ClientThread(clientInputStream);
			
			thread.start();				
		}
		catch(UnknownHostException e) 
		{	
			System.out.println(e);
			System.exit(1); 
		}
		catch(IOException e) 
		{	
			System.out.println(e);
			System.exit(1); 
		} 
	}

	void setUpChat(boolean loggedOn, String welcomeMessage)
	{	
		ButtonHandler bHandler = new ButtonHandler();
		
		// Remove initial GUI components (textfield, password field, logon button)
		container.remove(logonFieldsPanel);
		container.remove(logonButtonPanel);
		
		// If the friend has not logged on an error message will be displayed 
		if(!loggedOn)
		{	
			outputPanel = new JPanel();
			outputPanel.setBackground(Color.WHITE);
			// Add text area	
			outputArea = new JTextArea(12,30);
			outputArea.setEditable(false);
			outputArea.setLineWrap(true);
			outputArea.setWrapStyleWord(true);
			outputArea.setFont(new Font("Verdana", Font.BOLD, 11));
			// Add message to text area
			outputArea.setText(welcomeMessage);
			outputPanel.add(outputArea);
			outputPanel.add(new JScrollPane(outputArea));	
			container.add(outputPanel, BorderLayout.CENTER);
			setSize(375,300);
		}
		else
		{	
			messagePanel = new JPanel();
      		messagePanel.setLayout(new GridLayout(2,1));
      		
      		// GUI components for broadcast messages area
   	   		JPanel receiveMessagePanel = new JPanel();
    	  	receiveMessagePanel.setLayout(new BorderLayout());
      		receiveMessageArea = new JTextArea(15,30);
  	    	receiveMessageArea.setForeground(Color.GRAY);
			receiveMessageArea.setEditable(false);
			receiveMessageArea.setLineWrap(true);
			receiveMessageArea.setWrapStyleWord(true);
			receiveMessageArea.setFont(new Font("Verdana", Font.BOLD, 11));
			receiveMessagePanel.add(receiveMessageArea, BorderLayout.CENTER);
			receiveMessagePanel.add(new JScrollPane(receiveMessageArea), BorderLayout.CENTER);
			messagePanel.add(receiveMessagePanel);
			
      		JPanel sendMessagePanel = new JPanel();
      		sendMessagePanel.setLayout(new BorderLayout());
      	
 	     	sendMessageArea = new JTextArea(10,30);
 	     	sendMessageArea.setEditable(true);
 	     	sendMessageArea.setLineWrap(true);
 	     	sendMessageArea.setWrapStyleWord(true);
 	     	sendMessageArea.setForeground(Color.BLACK);
 	     	sendMessageArea.setFont(new Font("Verdana", Font.BOLD, 11));
	
			sendMessagePanel.add(sendMessageArea, BorderLayout.CENTER);
			sendMessagePanel.add(new JScrollPane(sendMessageArea), BorderLayout.CENTER);
		
			send = new JButton("send");
   			send.addActionListener(bHandler);
   			send.setEnabled(true); 			
   			sendMessagePanel.add(send, BorderLayout.SOUTH);
   			
   			messagePanel.add(sendMessagePanel);  			
   			container.add(messagePanel, BorderLayout.EAST);
		
			friendPanel = new JPanel();
    	  	friendPanel.setLayout(new BoxLayout(friendPanel, BoxLayout.PAGE_AXIS));
      		friendPanel.setBackground(Color.WHITE);
      		
      		arkenButton = new JButton("Arken");
      		arkenButton.addActionListener(bHandler);
      		arkenButton.setEnabled(false);
      		benButton = new JButton("Ben");
      		benButton.addActionListener(bHandler);
      		benButton.setEnabled(false);
      		darklarkButton = new JButton("Darklark");
      		darklarkButton.addActionListener(bHandler);
      		darklarkButton.setEnabled(false);
      		freeButton = new JButton("Free");
      		freeButton.addActionListener(bHandler);
      		freeButton.setEnabled(false);
      		groupButton = new JButton("Group");
      		groupButton.addActionListener(bHandler);
			groupButton.setEnabled(true);
			buttonPanel = new JPanel();
			buttonPanel.setBackground(Color.WHITE);
			buttonPanel.add(arkenButton);
			buttonPanel.add(benButton);
			buttonPanel.add(darklarkButton);
			buttonPanel.add(freeButton);
			buttonPanel.add(groupButton);
			
			friendPanel.add(buttonPanel);
			container.add(friendPanel, BorderLayout.CENTER);
   			
      		setSize(500,500);
      		
      		addToReceiveMessageArea(welcomeMessage);
		}
		setResizable(false);
		setVisible(true);
	}
	
	void sendLoginDetails()
	{	
		try
		{	
			// get username and password from text and password field 
			EncryptedMessage uname = new EncryptedMessage(username.getText());
			uname.encrypt();
			
			EncryptedMessage pword = new EncryptedMessage(new String (password.getPassword()));
			pword.encrypt();
			
			// and send details to server 
			clientOutputStream.writeObject(uname);
			clientOutputStream.writeObject(pword);
		}
		catch(IOException e) 
		{	
			System.out.println(e);
			System.exit(1);
		}
	}

	void addToReceiveMessageArea(String s)
	{	
		receiveMessageArea.append(s + "\n");
		receiveMessageArea.setCaretPosition(receiveMessageArea.getText().length());
	}

	void closeStreams()
	{	
		try
      	{	
      		// close input & output streams
      		clientOutputStream.close();
			clientInputStream.close();
      		
			socket.close();
		}
		catch(IOException e) 
		{	
			System.out.println(e);
			System.exit(1); 
		}
	}
	
	public void setAllButtonsGrey()
	{
		arkenButton.setForeground(new JButton().getForeground());
		benButton.setForeground(new JButton().getForeground());
		darklarkButton.setForeground(new JButton().getForeground());
		freeButton.setForeground(new JButton().getForeground());
		groupButton.setForeground(new JButton().getForeground());
	}
	
	private String getCompressedMessage(String messageToCompress)
	{
		CompressedMessage compressedMessage = new CompressedMessage(messageToCompress);
		compressedMessage.compress();
		return compressedMessage.getMessage();
	}
	
	private String getDecompressedMessage(String messageToDecompress)
	{
		CompressedMessage compressedMessage = new CompressedMessage(messageToDecompress);
		compressedMessage.decompress();
		return compressedMessage.getMessage();
	}
	
	// main method of class Client
	public static void main(String args[])
   	{			
		Client chatClient = new Client();
		chatClient.getConnections();
	}
	

/* -----------------------------------------------------------------------
 	beginning of class ClientThread
   ----------------------------------------------------------------------- */	
    
    private class ClientThread extends Thread 
	{	
    	ObjectInputStream threadInputStream;

		public ClientThread(ObjectInputStream in)
		{	
			// initialise input stream
			threadInputStream = in;
		}
	
		public void run()
		{	
			// when method start is called thread execution will begin in this method  
  		    try
			{	
				/* read Boolean value sent by server */
  		    	boolean loggedOn = (boolean)threadInputStream.readObject();

				/* based on Boolean value sent by server take action */
				
				if(!loggedOn)
				{ 	
					// call method to close input & output streams & socket
					closeStreams();
					
					// call method to display message
					
					setUpChat(loggedOn, "Logon unsuccessful");	
						
				}
				else
				{	
					@SuppressWarnings("unchecked")
					ArrayList<String> onlineFriendsFromServer = (ArrayList<String>) threadInputStream.readObject();
					for (String onlineFriend : onlineFriendsFromServer)
					{
						onlineFriends.add(onlineFriend);
					}
				
					String welcomeMessage = (String)threadInputStream.readObject();
					welcomeMessage = getDecompressedMessage(welcomeMessage);
				
					setUpChat(loggedOn, welcomeMessage);
					
					enableOnlineFriendsButtons();
					
					String message;
					
					boolean offline = false;
					while(!offline)
					{	
						message = (String)threadInputStream.readObject();
						message = getDecompressedMessage(message);
						
						String messagePrefix = "";
	  					if (message.contains(DELIMINATOR))
	  					{
	  						messagePrefix = getMessagePrefix(message);
	  						message = message.substring(messagePrefix.length() + DELIMINATOR.length());
	  					}
						
						if(messagePrefix.equals("goodbye"))
						{
							offline = true;
							System.out.println(message);
							disableGUI();
							closeStreams();
						}
						else if(messagePrefix.equals("join"))
						{
							addToReceiveMessageArea(message +" has joined");
							onlineFriends.add(message);
							enableOnlineFriendsButtons();
						}
						else if(messagePrefix.equals("quit"))
						{
							addToReceiveMessageArea(message +" has quit");
							onlineFriends.remove(message);
							disableOnlineFriendButton(message);
						}		
						else if(messagePrefix.equals("online"))
						{
							addToReceiveMessageArea("Your friends online: " +message);
							enableOnlineFriendsButtons();
						}
						else
						{
							addToReceiveMessageArea(message);
						}
					}
				}
			}
			catch(IOException e)
			{	System.out.println(e);
				System.exit(1);
			}	
			catch(ClassNotFoundException e)
			{	System.out.println(e);
				System.exit(1);
			}
		}
		
		private void enableOnlineFriendsButtons()
		{	
			for (String onlineFriend : onlineFriends)
			{
				if (onlineFriend.equals("Arken"))
				{
					arkenButton.setEnabled(true);
				}
				else if (onlineFriend.equals("Ben"))
				{
					benButton.setEnabled(true);
				}
				else if (onlineFriend.equals("Darklark"))
				{
					darklarkButton.setEnabled(true);
				}
				else if (onlineFriend.equals("Free"))
				{
					freeButton.setEnabled(true);
				}
			}
			groupButton.doClick();
		}
		
		private void disableOnlineFriendButton(String quitFriend)
		{
			System.out.println(quitFriend+"end");
			if (quitFriend.equals("Arken"))
			{
				arkenButton.setEnabled(false);
			}
			else if (quitFriend.equals("Ben"))
			{
				benButton.setEnabled(false);
			}
			else if (quitFriend.equals("Darklark"))
			{
				darklarkButton.setEnabled(false);
			}
			else if (quitFriend.equals("Free"))
			{
				freeButton.setEnabled(false);
			}
		}
		
		private String getMessagePrefix(String message)
   		{
   			String messagePrefix = "";
   			
   			if (message.startsWith("goodbye" +DELIMINATOR))
   			{
   				messagePrefix = "goodbye";
   			}
   			else if (message.startsWith("join" +DELIMINATOR))
   			{
   				messagePrefix = "join";
   			}
   			else if (message.startsWith("quit" +DELIMINATOR))
   			{
   				messagePrefix = "quit";
   			}
   			else if (message.startsWith("online" +DELIMINATOR))
   			{
   				messagePrefix = "online";
   			}
   			
   			return messagePrefix;
   		}
		
		private void disableGUI()
		{
			send.setEnabled(false);
			arkenButton.setEnabled(false);
			benButton.setEnabled(false);
			darklarkButton.setEnabled(false);
			freeButton.setEnabled(false);
			groupButton.setEnabled(false);
			sendMessageArea.setEnabled(false);			
		}
		
	} // end of class ClientThread
   
   	// beginning of class ButtonHandler - inner class for event handling
	private class ButtonHandler implements ActionListener
	{	
		public void actionPerformed(ActionEvent e)
		{	
			if(e.getSource() == send)
			{
				if (sendMessageArea.getText().equals(""))
				{
					return;
				}
				else
				{
					String messageToSend = (selectedButton +DELIMINATOR +sendMessageArea.getText());
					addToReceiveMessageArea("To " +selectedButton +": " +sendMessageArea.getText());
					
                    try
            		{	
            			clientOutputStream.writeObject(getCompressedMessage(messageToSend));
            		}
            		catch(IOException e1) 
            		{	
            			System.out.println(e1);
            			System.exit(1);
            		}
				}
				
				sendMessageArea.setText("");
			}
			
			if(e.getSource() == logonButton)
			{
				/* 	if the logon button has been clicked, call a method
			 	to send username and password to server */ 
				sendLoginDetails();
			}
			
			if(e.getSource() == arkenButton)
			{
				setAllButtonsGrey();
				arkenButton.setForeground(Color.RED);
				selectedButton = "Arken";
			}
			
			if(e.getSource() == benButton)
			{
				setAllButtonsGrey();
				benButton.setForeground(Color.RED);
				selectedButton = "Ben";
			}
			
			if(e.getSource() == darklarkButton)
			{
				setAllButtonsGrey();
				darklarkButton.setForeground(Color.RED);
				selectedButton = "Darklark";
			}
			
			if(e.getSource() == freeButton)
			{
				setAllButtonsGrey();
				freeButton.setForeground(Color.RED);
				selectedButton = "Free";
			}
			
			if(e.getSource() == groupButton)
			{
				setAllButtonsGrey();
				groupButton.setForeground(Color.RED);
				selectedButton = "Group";
			}
		}
		
	}
	
}
	