import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//import EncryptedMessage;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
/**
 * 
 * @author James Anderson 
 *
 */
public class Server extends JFrame {
	 
	private static final long serialVersionUID = 1L;

	final int NO_OF_FRIENDS = 4;
	final String DELIMINATOR = "> ";

	ArrayList<ServerThread> onlineFriendThreads = new ArrayList<ServerThread>();
	ArrayList<String> onlineNames = new ArrayList<String>();
	ArrayList<String> onlineFriends = new ArrayList<String>();
	
	JTextArea outputArea;
	
	ServerSocket serverSocket;
	Socket client;
	ObjectInputStream serverInputStream;
	ObjectOutputStream serverOutputStream;
	
	public Server()
	{	
		super("Chat Service - Server");
		addWindowListener
		(	
				new WindowAdapter()
			{	
					public void windowClosing(WindowEvent e)
				{	
						System.exit(0);
				}
			}
		);
			
		try
		{
			serverSocket = new ServerSocket(6000);			 	
		}
		catch(IOException e)
		{	
			System.out.println(e);
			System.exit(1);
		}
		
		// create and add GUI components
		Container container = getContentPane();
		container.setLayout(new FlowLayout());
		
		// Add text output area
		outputArea = new JTextArea(18,30);
		outputArea.setEditable(false);
		outputArea.setLineWrap(true);
		outputArea.setWrapStyleWord(true);
		outputArea.setFont(new Font("Verdana", Font.BOLD, 11));
		container.add(outputArea);
		container.add(new JScrollPane(outputArea));
		
		setSize(400,320);
		setResizable(false);
		setVisible(true);
	}
	
	void getFriends()
	{	
		addOutput("Server is up and waiting for a connection...");
		
		// Set up arrays containing valid usernames and passwords
		String usernames[] = {"Adam Smyth", "Bill Allen", "Cathy Clark", "Davina Doe"};
		String passwords[] = {"LuQezz169", "amG4tyz", "Dw1wU9wy", "Fre195Ufm"};
		/*String usernames[] = {"one", "three", "five", "seven"};//these are easy
		String passwords[] = {"two", "four", "six", "eight"};//these are easy*/
		String tags[] = {"Arken", "Ben", "Darklark", "Free"};
			
		boolean friendLoggedOn[] = new boolean[NO_OF_FRIENDS];		
		 	
		int friendCount = 0;
		while(friendCount < NO_OF_FRIENDS)
		{	
			try
			{	
				client = serverSocket.accept();
				
				serverInputStream = new ObjectInputStream(client.getInputStream());
				serverOutputStream = new ObjectOutputStream(client.getOutputStream());
				
				EncryptedMessage uname = (EncryptedMessage)serverInputStream.readObject();
				EncryptedMessage pword = (EncryptedMessage)serverInputStream.readObject();
				
				addOutput("\nLogin Details Received\n----------------------------------------");
				addOutput("encrypted username : " + uname.getMessage());
				addOutput("encrypted password : " + pword.getMessage());
				
				uname.decrypt();
				pword.decrypt();
				
				addOutput("decrypted username : " + uname.getMessage());
				addOutput("decrypted password : " + pword.getMessage());
								
				int pos = -1;
				boolean found = false;
				
				// Comparing usernames and passwords with arrays
				for(int i = 0; i < usernames.length; i++)
				{	
					if(uname.getMessage().equals(usernames[i]) && pword.getMessage().equals(passwords[i]) && !friendLoggedOn[i])
					{	
						friendLoggedOn[i] = true;
						found = true;
						pos = i;
					}
				}
				
				addOutput("Login details received from client " + (pos + 1) +", " +uname.getMessage());
				
				if(found)
				{	
					serverOutputStream.writeObject(new Boolean(true));
					
					addOutput("Login details received from client " +(pos + 1) +", " +uname.getMessage() +" are valid");
					addOutput("Client " +uname.getMessage() +" is known as " +tags[pos]);
					addOutput(uname.getMessage() +" known as " +tags[pos] +" has joined");
					
					ServerThread friend = new ServerThread(serverInputStream, serverOutputStream, tags[pos], friendCount);
					onlineFriendThreads.add(friend);
					friend.start();
					
					onlineNames.add(usernames[pos]);
					friendCount++;
				}
				else
				{
					serverOutputStream.writeObject(new Boolean(false));
					addOutput("Login details received from client " +(pos + 1) +", " +uname.getMessage() +" are false");
				}
			}
			catch(IOException e)
			{	System.out.println(e);
				System.exit(1);
			}
			catch(ClassNotFoundException e)
			{	System.out.println(e);
				System.exit(1);
			}
		}
	}
	
	void addOutput(String s)
	{	
		// Add message to text output area
		outputArea.append(s + "\n");
		outputArea.setCaretPosition(outputArea.getText().length());
	}
	
	private String getCompressedMessage(String messageToCompress)
	{
		addOutput("Message before compression: " +messageToCompress);
		CompressedMessage compressedMessage = new CompressedMessage(messageToCompress);
		compressedMessage.compress();
		addOutput("Message after compression: " +compressedMessage.getMessage());
		return compressedMessage.getMessage();
	}
	
	private String getDecompressedMessage(String messageToDecompress)
	{
		addOutput("Message before decompression: " +messageToDecompress);
		CompressedMessage compressedMessage = new CompressedMessage(messageToDecompress);
		compressedMessage.decompress();
		addOutput("Message after decompression: " +compressedMessage.getMessage());
		return compressedMessage.getMessage();
	}

	// main method of class Server
	public static void main(String args[])
	{	
		Server gameServer = new Server();
		gameServer.getFriends();
	}
	
/* -----------------------------------------------------------------------
 	beginning of class ServerThread
   ----------------------------------------------------------------------- */	
 		
	private class ServerThread extends Thread 
	{	
		ObjectInputStream threadInputStream;
		ObjectOutputStream threadOutputStream;
		String friendTag;
		int friendPosition; 

		public ServerThread(ObjectInputStream in, ObjectOutputStream out, String name, int num)
		{	
			threadInputStream = in;
			threadOutputStream = out;
			friendTag = name;
			friendPosition = num;
  		}
  	
  		@SuppressWarnings("static-access")
		void goToSleep()
  		{	
  			try
			{	// This thread will sleep for 1000 milliseconds
				this.sleep(1000);
			}
			catch(InterruptedException e)
			{	System.out.println(e);
				System.exit(1);
			}
  		}
  
  		public void run()
  		{	
  			try
  			{	
  				boolean offline = false;
  				String message;
  				
  				threadOutputStream.writeObject(onlineFriends);
  				
  				String welcomeMessage = "Welcome to the chat server " +onlineNames.get(friendPosition) +"\nYour friends online: " +getFriendsOnline();				
  				threadOutputStream.writeObject(getCompressedMessage(welcomeMessage));
  				
				onlineFriends.add(friendTag);
  				
  				sendMessageToOtherFriends("join" +DELIMINATOR +friendTag);
  				
  				while(!offline)
  				{
  					message = (String)threadInputStream.readObject();
  					message = getDecompressedMessage(message);
  					
  					String messagePrefix = getMessagePrefix(message);
  					message = message.substring(messagePrefix.length() + DELIMINATOR.length());
  					
  					if(message.equals("quit"))
  					{
  						offline = true;
  						sendMessageToOtherFriends("quit" +DELIMINATOR +friendTag);
  						threadOutputStream.writeObject(getCompressedMessage("goodbye" +DELIMINATOR +"You have quit"));
  						onlineFriends.remove(friendPosition);
  					}
  					else if(message.equals("online"))
  					{
  						sendFriendsOnline();
  					}
  					else
  					{
  						sendMessageToRecipient(messagePrefix, message);
  					}
  					
  					goToSleep();
  				}
  			}
			catch(IOException e)
			{	System.out.println(e);
				System.exit(1);
			}
			catch(ClassNotFoundException e)
			{	System.out.println(e);
				System.exit(1);
			}	
   		}
  	
   		private void sendMessageToOtherFriends(String message)
   		{	
   			synchronized(onlineFriendThreads)
			{
   				for (int i = 0; i < onlineNames.size(); i++)
	   			{
	   				if (friendTag != onlineFriendThreads.get(i).friendTag)
	   				{
	   					try
						{	
							onlineFriendThreads.get(i).threadOutputStream.writeObject(getCompressedMessage(message));
						}
						catch(IOException e)
						{	
							System.out.println(e);
							System.exit(1);
						}
	   				}
	   			}
			}
   		}
   		
   		private void sendMessageToRecipient(String recipient, String message)
   		{
   			if (recipient.equals("Group"))
   			{
   				sendMessageToOtherFriends(friendTag +": " +message);
   			}
   			else
   			{
   				for (int i = 0; i < onlineNames.size(); i++)
   	   			{
   	   				if (recipient.equals(onlineFriendThreads.get(i).friendTag))
   	   				{
   	   					try
   						{	
   							onlineFriendThreads.get(i).threadOutputStream.writeObject(getCompressedMessage(friendTag +": " +message));
   						}
   						catch(IOException e)
   						{	
   							System.out.println(e);
   							System.exit(1);
   						}
   	   				}
   	   			}
   			}
   		}
   		
   		private String getFriendsOnline()
   		{
   			String friendsOnline = "";
   			
   			if (onlineFriends.isEmpty())
   			{
   				friendsOnline = "none";
   			}
   			else
   			{
   				for (int i = 0; i < onlineFriends.size(); i++)
   				{
   					friendsOnline += onlineFriends.get(i) + ", ";
   				}
   				friendsOnline = friendsOnline.substring(0, friendsOnline.length() - 2);
   			}
   		
   			return friendsOnline;
   		}
   		
   		private void sendFriendsOnline()
   		{
   			String friendsOnline = "";
   			
   			if (onlineFriends.size() == 1)
   			{
   				friendsOnline = "none";
   			}
   			else
   			{
   				for (int i = 0; i < onlineFriends.size(); i++)
   				{
   					if (friendTag != onlineFriends.get(i))
   					{
   						friendsOnline += onlineFriends.get(i) + ", ";
   					}
   				}
   				friendsOnline = friendsOnline.substring(0, friendsOnline.length() - 2);
   			}
   			try
   			{
				threadOutputStream.writeObject("online" +DELIMINATOR +friendsOnline);
			}
   			catch (IOException e)
   			{
				e.printStackTrace();
			}
   		}
   		
   		private String getMessagePrefix(String message)
   		{
   			String messagePrefix = "";
   			
   			if (message.startsWith("Group" +DELIMINATOR))
   			{
   				messagePrefix = "Group";
   			}
   			else
   			{
   				for (String onlineFriend : onlineFriends)
   	   			{
   	   				if (message.startsWith(onlineFriend +DELIMINATOR))
   	   				{
   	   					messagePrefix = message.substring(0, onlineFriend.length());
   	   				}
   	   			}
   			}	
   			
   			return messagePrefix;
   		}
   		//TODO Method to close Streams
	}
	
}
